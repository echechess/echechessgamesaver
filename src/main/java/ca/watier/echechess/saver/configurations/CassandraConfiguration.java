/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.configurations;

import ca.watier.echechess.saver.cassandra.Keyspace;
import ca.watier.echechess.saver.cassandra.Node;
import ca.watier.echechess.saver.cassandra.Table;
import ca.watier.echechess.saver.cassandra.TableColumn;
import ca.watier.echechess.saver.cassandra.containers.Columns;
import ca.watier.echechess.saver.cassandra.containers.KeyspaceReplication;
import ca.watier.echechess.saver.cassandra.enums.ColumnType;
import ca.watier.echechess.saver.cassandra.enums.KeyspaceReplicationMode;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Optional;

@Configuration
public class CassandraConfiguration {

    private static final short CASSANDRA_DEFAULT_PORT = 7199;
    private final Environment environment;

    @Autowired
    public CassandraConfiguration(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public Session session(Node node) {
        return node.getSession();
    }

    @Bean
    public Node node() {
        String ip = environment.getProperty("node.cassandra.ip");
        Short port = Optional.ofNullable(environment.getProperty("node.cassandra.port", Short.class)).
                orElse(CASSANDRA_DEFAULT_PORT);

        Node node = new Node();
        node.connect(ip, port);
        return node;
    }

    @Bean
    public Keyspace gameKeyspace(KeyspaceReplication gameKeyspaceReplication) {
        Keyspace keyspace = new Keyspace("eckeyspace", true);
        keyspace.setKeyspaceReplication(gameKeyspaceReplication);
        return keyspace;
    }

    @Bean
    public KeyspaceReplication gameKeyspaceReplication() {
        KeyspaceReplication keyspaceReplication = new KeyspaceReplication(KeyspaceReplicationMode.SIMPLE_STRATEGY);
        keyspaceReplication.updateReplicationMode(1);
        return keyspaceReplication;
    }

    @Bean
    public Columns ecGameColumns() {
        Columns tableColumns = new Columns();
        tableColumns.add(new TableColumn("uuid", "uuid", ColumnType.PRIMARY_KEY));
        tableColumns.add(new TableColumn("data", "blob"));
        return tableColumns;
    }

    @Bean
    public Table gameTable(Keyspace gameKeyspace, Columns ecGameColumns) {
        Table ecgames = new Table(gameKeyspace, "ecgames", true);
        ecgames.setColumns(ecGameColumns);
        return ecgames;
    }
}
