/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.services;

import ca.watier.echechess.communication.redis.interfaces.GameRepository;
import ca.watier.echechess.communication.redis.model.GenericGameHandlerWrapper;
import ca.watier.echechess.engine.engines.GenericGameHandler;
import ca.watier.echechess.saver.cassandra.CassandraRepository;
import ca.watier.echechess.saver.cassandra.Keyspace;
import ca.watier.echechess.saver.cassandra.Node;
import ca.watier.echechess.saver.cassandra.Table;
import com.datastax.driver.core.Session;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class DatabaseService {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DatabaseService.class);
    private final GameRepository<GenericGameHandler> gameRepository;

    private final Node node;
    private final Table gameTable;
    private final Keyspace gameKeyspace;
    private final CassandraRepository repository;

    @Autowired
    public DatabaseService(
            GameRepository<GenericGameHandler> gameRepository, Node node, Table gameTable, Keyspace gameKeyspace, CassandraRepository repository) {
        this.gameRepository = gameRepository;
        this.node = node;
        this.gameTable = gameTable;
        this.gameKeyspace = gameKeyspace;
        this.repository = repository;
    }

    public void save(String uuid) {
        if (StringUtils.isBlank(uuid)) {
            return;
        }

        GenericGameHandlerWrapper<GenericGameHandler> handlerWrapper = gameRepository.get(uuid);

        if (handlerWrapper == null) {
            throw new IllegalStateException(String.format("The game cannot be null! -> %s", uuid));
        }

        byte[] serialized = SerializationUtils.serialize(handlerWrapper.getGenericGameHandler());

        repository.insert(gameKeyspace, gameTable, uuid, "0x" + Hex.encodeHexString(serialized));
        LOGGER.info("Game saved {}", uuid);
    }

    @EventListener(classes = {ApplicationReadyEvent.class})
    public void whenApplicationReadyEvent() {
        initEnvironment();
    }

    private void initEnvironment() {
        Session session = node.getSession();
        session.execute(gameKeyspace.toString());
        session.execute(gameTable.toString());
    }
}
