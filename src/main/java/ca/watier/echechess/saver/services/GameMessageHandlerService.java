/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameMessageHandlerService {
    private final DatabaseService databaseService;

    @Autowired
    public GameMessageHandlerService(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    @RabbitListener(queues = "#{nodeToAppMoveQueue.name}")
    public void handleMoveResponseMessage(String message) {

        if (StringUtils.isBlank(message)) {
            return;
        }

        // uuid, from, to, MoveType, PlayerSide
        String[] sections = message.split("\\|");
        databaseService.save(sections[0]);
    }
}
