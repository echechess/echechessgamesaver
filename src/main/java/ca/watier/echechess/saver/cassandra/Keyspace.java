/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra;

import ca.watier.echechess.saver.cassandra.containers.KeyspaceReplication;
import ca.watier.echechess.saver.cassandra.exceptions.KeyspaceReplicationNotDefinedException;

import java.util.Objects;

public class Keyspace {
    private static final String CREATE_KEYSPACE = "CREATE KEYSPACE";
    private static final String CREATE_KEYSPACE_IF_NOT_EXISTS = CREATE_KEYSPACE + " IF NOT EXISTS";
    private final String name;
    private final boolean createOnlyIfExist;
    private boolean durableWrite;
    private KeyspaceReplication keyspaceReplication;

    public Keyspace(String name, boolean createOnlyIfExist, boolean durableWrite) {
        this.name = name;
        this.createOnlyIfExist = createOnlyIfExist;
        this.durableWrite = durableWrite;
    }

    public Keyspace(String name, boolean createOnlyIfExist) {
        this.name = name;
        this.createOnlyIfExist = createOnlyIfExist;
        this.durableWrite = true;
    }

    public void setKeyspaceReplication(KeyspaceReplication keyspaceReplication) {
        this.keyspaceReplication = keyspaceReplication;
    }

    public String getName() {
        return name;
    }

    public boolean isCreateOnlyIfExist() {
        return createOnlyIfExist;
    }

    public boolean isDurableWrite() {
        return durableWrite;
    }

    public void setDurableWrite(boolean durableWrite) {
        this.durableWrite = durableWrite;
    }

    @Override
    public String toString() {
        String beginning = createOnlyIfExist ? CREATE_KEYSPACE_IF_NOT_EXISTS : CREATE_KEYSPACE;

        if (Objects.isNull(keyspaceReplication)) {
            throw new KeyspaceReplicationNotDefinedException();
        }

        return String.format("%s %s\n" +
                        " %s\n" +
                        "AND DURABLE_WRITES = %s;",
                beginning, name, keyspaceReplication, durableWrite);
    }
}
