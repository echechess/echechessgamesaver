/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra;

import ca.watier.echechess.saver.cassandra.containers.Columns;
import ca.watier.echechess.saver.cassandra.containers.PrimaryKeys;
import ca.watier.echechess.saver.cassandra.enums.ColumnType;
import ca.watier.echechess.saver.cassandra.enums.TableOrderBy;
import ca.watier.echechess.saver.cassandra.exceptions.ColumnNotFoundException;
import ca.watier.echechess.saver.cassandra.exceptions.NoPrimaryKeyException;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Table implements Serializable {
    private static final long serialVersionUID = -1737890461893049835L;

    private static final String BASE_TABLE_CREATE_PATTERN = "%s %s.%s (\n" +
            "\t%s,\n" +
            "\tPRIMARY KEY (%s)\n" +
            ")";

    private static final String CREATE_TABLE = "CREATE TABLE";
    private static final String CREATE_IF_NOT_EXIST_TABLE = "CREATE TABLE IF NOT EXISTS";

    private String keyspaceName;
    private String tableName;
    private boolean createOnlyIfExist;
    private PrimaryKeys primaryKeys = new PrimaryKeys();
    private TableOrderBy orderByType = TableOrderBy.NONE;
    private String orderBy;
    private Columns columns = new Columns();

    public Table(String keyspaceName, String tableName, boolean createOnlyIfExist) {
        this.keyspaceName = keyspaceName;
        this.tableName = tableName;
        this.createOnlyIfExist = createOnlyIfExist;
    }

    public Table(Keyspace keyspace, String tableName, boolean createOnlyIfExist) {
        this.keyspaceName = keyspace.getName();
        this.tableName = tableName;
        this.createOnlyIfExist = createOnlyIfExist;
    }

    public void setColumns(Columns columns) {
        if (Objects.isNull(columns)) {
            return;
        }

        for (TableColumn column : columns) {
            if (ColumnType.PRIMARY_KEY.equals(column.getColumnType())) {
                this.primaryKeys.add(column.getName());
            }
        }

        this.columns = columns;
    }

    public String getFormattedColumns() {
        List<String> values = new ArrayList<>();

        for (TableColumn column : columns) {
            values.add(column.getName());
        }

        return String.join(",", values);
    }

    public void orderBy(String orderBy, TableOrderBy orderByType) {
        if (Objects.isNull(orderBy) || Objects.isNull(orderByType)) {
            return;
        }

        this.orderBy = orderBy;
        this.orderByType = orderByType;
    }

    public String getName() {
        return tableName;
    }

    @Override
    public String toString() {

        if (primaryKeys.isEmpty()) {
            throw new NoPrimaryKeyException();
        }

        String createTableType = (createOnlyIfExist ? CREATE_IF_NOT_EXIST_TABLE : CREATE_TABLE);

        StringBuilder builder = new StringBuilder(String.format(BASE_TABLE_CREATE_PATTERN, createTableType, keyspaceName, tableName, columns, primaryKeys));

        switch (orderByType) {
            case ASC:
            case DESC:
                validateOrderBy();
                builder.append(" WITH CLUSTERING ORDER BY (").append(orderBy).append(" ").append(orderByType).append(");");
                break;
            default:
            case NONE:
                builder.append(";");
                break;
        }

        return builder.toString();
    }

    private void validateOrderBy() {
        boolean isFound = false;

        for (TableColumn column : columns) {
            isFound |= StringUtils.equals(column.getName(), orderBy);
        }

        if (!isFound) {
            throw new ColumnNotFoundException();
        }
    }
}
