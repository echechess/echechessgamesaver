/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra.containers;

import ca.watier.echechess.saver.cassandra.enums.KeyspaceReplicationMode;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import static ca.watier.echechess.saver.cassandra.enums.KeyspaceReplicationMode.SIMPLE_STRATEGY;

public class KeyspaceReplication extends HashMap<String, Integer> {
    private static final long serialVersionUID = -769485762184771696L;
    private static final String REPLICATION_FACTOR_KEY = "replication_factor";

    private final KeyspaceReplicationMode keyspaceReplicationMode;

    public KeyspaceReplication(KeyspaceReplicationMode keyspaceReplicationMode) {
        if (Objects.isNull(keyspaceReplicationMode)) {
            throw new IllegalStateException("The keyspace replication mode cannot be null!");
        }

        if (isSimpleStrategy(keyspaceReplicationMode)) {
            put(REPLICATION_FACTOR_KEY, 0);
        }

        this.keyspaceReplicationMode = keyspaceReplicationMode;
    }

    private boolean isSimpleStrategy(KeyspaceReplicationMode keyspaceReplicationMode) {
        return SIMPLE_STRATEGY.equals(keyspaceReplicationMode);
    }

    @Override
    public Integer put(String key, Integer value) {
        validateIfEditValid();
        return super.put(key, value);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Integer> m) {
        validateIfEditValid();
        super.putAll(m);
    }

    @Override
    public Integer remove(Object key) {
        validateIfEditValid();
        return super.remove(key);
    }

    @Override
    public void clear() {
        validateIfEditValid();
        super.clear();
    }

    @Override
    public Integer putIfAbsent(String key, Integer value) {
        validateIfEditValid();
        return super.putIfAbsent(key, value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        validateIfEditValid();
        return super.remove(key, value);
    }

    @Override
    public Integer computeIfAbsent(String key, Function<? super String, ? extends Integer> mappingFunction) {
        validateIfEditValid();
        return super.computeIfAbsent(key, mappingFunction);
    }

    @Override
    public Integer computeIfPresent(String key, BiFunction<? super String, ? super Integer, ? extends Integer> remappingFunction) {
        validateIfEditValid();
        return super.computeIfPresent(key, remappingFunction);
    }

    @Override
    public Integer compute(String key, BiFunction<? super String, ? super Integer, ? extends Integer> remappingFunction) {
        validateIfEditValid();
        return super.compute(key, remappingFunction);
    }

    @Override
    public Integer merge(String key, Integer value, BiFunction<? super Integer, ? super Integer, ? extends Integer> remappingFunction) {
        validateIfEditValid();
        return super.merge(key, value, remappingFunction);
    }

    @Override
    public void replaceAll(BiFunction<? super String, ? super Integer, ? extends Integer> function) {
        validateIfEditValid();
        super.replaceAll(function);
    }

    private void validateIfEditValid() {
        if (isSimpleStrategy(keyspaceReplicationMode) && !isEmpty()) {
            throw new UnsupportedOperationException("Cannot add or remove item(s) while in 'SimpleStrategy' mode!");
        }
    }

    public Integer updateReplicationMode(int value) {
        return replace(REPLICATION_FACTOR_KEY, value);
    }

    public int getReplicationMode() {
        return get(REPLICATION_FACTOR_KEY);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("WITH REPLICATION = {\n")
                .append("'class': '")
                .append(keyspaceReplicationMode)
                .append("',\n");


        for (Iterator<Entry<String, Integer>> iterator = this.entrySet().iterator(); iterator.hasNext(); ) {
            Entry<String, Integer> integerEntry = iterator.next();

            builder.append("'")
                    .append(integerEntry.getKey())
                    .append("': ")
                    .append(integerEntry.getValue());

            if (iterator.hasNext()) {
                builder.append(",");
            }
        }

        builder.append("\n}");
        return builder.toString();
    }
}