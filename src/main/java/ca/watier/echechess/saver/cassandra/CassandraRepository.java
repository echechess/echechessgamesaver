/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra;

import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.Instant;

@Repository
public class CassandraRepository {

    private static final String INSERT = "INSERT INTO %s.%s (%s) VALUES (%s);";
    private static final String INSERT_IF_NOT_EXISTS = "INSERT INTO %s.%s (%s) VALUES (%s) IF NOT EXISTS;";
    private static final String INSERT_IF_NOT_EXISTS_TTL = "INSERT INTO %s.%s (%s) VALUES (%s) IF NOT EXISTS USING TTL %d;";
    private static final String INSERT_TTL_TIMESTAMP = "INSERT INTO %s.%s (%s) VALUES (%s) USING TTL %d AND TIMESTAMP %d;";
    private static final String INSERT_TTL = "INSERT INTO %s.%s (%s) VALUES (%s) USING TTL %d;";
    private static final String INSERT_TIMESTAMP = "INSERT INTO %s.%s (%s) VALUES (%s) USING TIMESTAMP %d;";
    private final Session session;

    @Autowired
    public CassandraRepository(Session session) {
        this.session = session;
    }

    public void insert(Keyspace keyspace, Table table, String... values) {
        session.execute(String.format(INSERT, keyspace.getName(), table.getName(), table.getFormattedColumns(), String.join(",", values)));
    }

    public void insertIfNotExists(Keyspace keyspace, Table table, String... values) {
        session.execute(String.format(INSERT_IF_NOT_EXISTS, keyspace.getName(), table.getName(), table.getFormattedColumns(), String.join(",", values)));
    }

    public void insertIfNotExistsWithTtl(Keyspace keyspace, Table table, int ttlDuration, String... values) {
        session.execute(String.format(INSERT_IF_NOT_EXISTS_TTL, keyspace.getName(), table.getName(), table.getFormattedColumns(), String.join(",", values), ttlDuration));
    }

    public void insertWithTtlAndEpoch(Keyspace keyspace, Table table, int ttlDuration, Instant epoch, String... values) {
        session.execute(String.format(INSERT_TTL_TIMESTAMP, keyspace.getName(), table.getName(), table.getFormattedColumns(), String.join(",", values), ttlDuration, epoch.toEpochMilli()));
    }

    public void insertWithTtl(Keyspace keyspace, Table table, int ttlDuration, String... values) {
        session.execute(String.format(INSERT_TTL, keyspace.getName(), table.getName(), table.getFormattedColumns(), String.join(",", values), ttlDuration));
    }

    public void insert(Keyspace keyspace, Table table, Instant epoch, String... values) {
        session.execute(String.format(INSERT_TIMESTAMP, keyspace.getName(), table.getName(), table.getFormattedColumns(), String.join(",", values), epoch.toEpochMilli()));
    }
}
