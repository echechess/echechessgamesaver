/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra;

import ca.watier.echechess.saver.cassandra.enums.ColumnType;

import java.io.Serializable;
import java.util.Objects;

public class TableColumn implements Serializable {
    private static final long serialVersionUID = 7889198085271851792L;
    private static final String DELIMITER = " ";

    private final String name;
    private final String cqlType;
    private final ColumnType columnType;

    public TableColumn(String name, String cqlType, ColumnType columnType) {
        this.name = name;
        this.cqlType = cqlType;
        this.columnType = columnType;
    }

    public TableColumn(String name, String cqlType) {
        this.name = name;
        this.cqlType = cqlType;
        this.columnType = ColumnType.NONE;
    }

    public String getName() {
        return name;
    }

    public String getCqlType() {
        return cqlType;
    }

    public ColumnType getColumnType() {
        return columnType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cqlType, columnType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableColumn that = (TableColumn) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(cqlType, that.cqlType) &&
                columnType == that.columnType;
    }

    @Override
    public String toString() {
        return String.join(DELIMITER, name, cqlType);
    }
}
