/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra;

import ca.watier.echechess.saver.cassandra.containers.KeyspaceReplication;
import ca.watier.echechess.saver.cassandra.enums.KeyspaceReplicationMode;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

public class KeyspaceReplicationTest {

    private KeyspaceReplication keyspaceReplicationWithSimpleStrat;
    private KeyspaceReplication keyspaceReplicationWithTopology;

    @Before
    public void setUp() {
        keyspaceReplicationWithSimpleStrat = new KeyspaceReplication(KeyspaceReplicationMode.SIMPLE_STRATEGY);
        keyspaceReplicationWithTopology = new KeyspaceReplication(KeyspaceReplicationMode.NETWORK_TOPOLOGY_STRATEGY);
    }

    @Test
    public void updateSimpleStrategyValue() {
        keyspaceReplicationWithSimpleStrat.updateReplicationMode(10);

        assertThat(keyspaceReplicationWithSimpleStrat.getReplicationMode()).isEqualTo(10);
    }

    @Test
    public void putWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.put("", 0);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void putAllWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.putAll(new HashMap<>());
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void removeWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.remove("");
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void removeKeyValueWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.remove("", 0);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void clearWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.clear();
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void putIfAbsentWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.putIfAbsent("", 0);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void computeIfAbsentWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.computeIfAbsent("", String::hashCode);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void computeIfPresentWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.computeIfPresent("", (s, integer) -> 0);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void computeWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.put("", 0);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void mergeWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.put("", 0);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void replaceAllWithSimpleStrategy() {
        try {
            keyspaceReplicationWithSimpleStrat.put("", 0);
            fail();
        } catch (UnsupportedOperationException ignored) {
        }
    }

    @Test
    public void updateReplicationMode() {
        assertThat(keyspaceReplicationWithSimpleStrat.toString()).hasToString("WITH REPLICATION = {\n" +
                "'class': 'SimpleStrategy',\n" +
                "'replication_factor': 0\n" +
                "}");

        keyspaceReplicationWithTopology.put("boston", 3);
        keyspaceReplicationWithTopology.put("seattle", 2);
        keyspaceReplicationWithTopology.put("tokyo", 1);
        assertThat(keyspaceReplicationWithTopology.toString()).hasToString("WITH REPLICATION = {\n" +
                "'class': 'NetworkTopologyStrategy',\n" +
                "'boston': 3,'tokyo': 1,'seattle': 2\n" +
                "}");
    }
}