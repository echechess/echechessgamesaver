/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra;

import ca.watier.echechess.saver.cassandra.containers.Columns;
import ca.watier.echechess.saver.cassandra.enums.ColumnType;
import ca.watier.echechess.saver.cassandra.enums.TableOrderBy;
import ca.watier.echechess.saver.cassandra.exceptions.ColumnNotFoundException;
import ca.watier.echechess.saver.cassandra.exceptions.NoPrimaryKeyException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

public class TableTest {

    @Test
    public void addColumnsWithoutException() {
        Table table = new Table("superKeyspace", "SuperTableName", false);
        Columns tableColumns = new Columns();
        tableColumns.add(new TableColumn("col_1", "integer", ColumnType.PRIMARY_KEY));
        tableColumns.add(new TableColumn("col_2", "string", ColumnType.STATIC));

        table.setColumns(tableColumns);

        try {
            assertThat(table).hasToString("CREATE TABLE superKeyspace.SuperTableName (\n" +
                    "\tcol_1 integer,col_2 string,\n" +
                    "\tPRIMARY KEY (col_1)\n" +
                    ");");
        } catch (NoPrimaryKeyException npke) {
            fail(npke.getMessage());
        }
    }

    @Test
    public void addColumnsWithException() {
        Table table = new Table("superKeyspace", "SuperTableName", false);
        Columns tableColumns = new Columns();
        tableColumns.add(new TableColumn("col_1", "integer"));
        tableColumns.add(new TableColumn("col_2", "string", ColumnType.STATIC));

        table.setColumns(tableColumns);

        try {
            System.out.println(table.toString());
            fail("No primary key(s)");
        } catch (NoPrimaryKeyException ignored) {
        }
    }

    @Test
    public void addColumnsAndCreateOnlyIfNotPresentWithoutException() {
        Table table = new Table("superKeyspace", "SuperTableName", true);
        Columns tableColumns = new Columns();
        tableColumns.add(new TableColumn("col_1", "integer", ColumnType.PRIMARY_KEY));
        tableColumns.add(new TableColumn("col_2", "string", ColumnType.STATIC));
        table.setColumns(tableColumns);

        try {
            assertThat(table).hasToString("CREATE TABLE IF NOT EXISTS superKeyspace.SuperTableName (\n" +
                    "\tcol_1 integer,col_2 string,\n" +
                    "\tPRIMARY KEY (col_1)\n" +
                    ");");
        } catch (NoPrimaryKeyException npke) {
            fail(npke.getMessage());
        }
    }

    @Test
    public void addColumnsAndCreateOnlyIfNotPresentWithOrderAsc() {
        Table table = new Table("superKeyspace", "SuperTableName", true);
        table.orderBy("col_1", TableOrderBy.ASC);
        Columns tableColumns = new Columns();
        tableColumns.add(new TableColumn("col_1", "integer", ColumnType.PRIMARY_KEY));
        tableColumns.add(new TableColumn("col_2", "string", ColumnType.STATIC));
        table.setColumns(tableColumns);

        try {
            assertThat(table).hasToString("CREATE TABLE IF NOT EXISTS superKeyspace.SuperTableName (\n" +
                    "\tcol_1 integer,col_2 string,\n" +
                    "\tPRIMARY KEY (col_1)\n" +
                    ") WITH CLUSTERING ORDER BY (col_1 ASC);");
        } catch (NoPrimaryKeyException npke) {
            fail(npke.getMessage());
        }
    }

    @Test
    public void addColumnsAndCreateOnlyIfNotPresentWithOrderDesc() {
        Table table = new Table("superKeyspace", "SuperTableName", true);
        table.orderBy("col_2", TableOrderBy.DESC);
        Columns tableColumns = new Columns();
        tableColumns.add(new TableColumn("col_1", "integer", ColumnType.PRIMARY_KEY));
        tableColumns.add(new TableColumn("col_2", "string", ColumnType.STATIC));
        table.setColumns(tableColumns);

        try {
            assertThat(table).hasToString("CREATE TABLE IF NOT EXISTS superKeyspace.SuperTableName (\n" +
                    "\tcol_1 integer,col_2 string,\n" +
                    "\tPRIMARY KEY (col_1)\n" +
                    ") WITH CLUSTERING ORDER BY (col_2 DESC);");
        } catch (NoPrimaryKeyException npke) {
            fail(npke.getMessage());
        }
    }


    @Test
    public void addColumnsAndCreateOnlyIfNotPresentWithOrderDescColNotFound() {
        Table table = new Table("superKeyspace", "SuperTableName", true);
        table.orderBy("col_3", TableOrderBy.DESC);
        Columns tableColumns = new Columns();
        tableColumns.add(new TableColumn("col_1", "integer", ColumnType.PRIMARY_KEY));
        tableColumns.add(new TableColumn("col_2", "string", ColumnType.STATIC));
        table.setColumns(tableColumns);

        try {
            String ignored = table.toString();
            fail("The column col_3 does not exist, supposed to fail!");
        } catch (ColumnNotFoundException ignored) {
        }
    }
}