/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.saver.cassandra;

import ca.watier.echechess.saver.cassandra.containers.KeyspaceReplication;
import ca.watier.echechess.saver.cassandra.enums.KeyspaceReplicationMode;
import ca.watier.echechess.saver.cassandra.exceptions.KeyspaceReplicationNotDefinedException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

public class KeyspaceTest {

    @Test
    public void KeyspaceCreateOnlyIfExistAndTenReplicasKeyspace() {
        Keyspace keyspace = new Keyspace("superName", true);
        KeyspaceReplication keyspaceReplication = new KeyspaceReplication(KeyspaceReplicationMode.SIMPLE_STRATEGY);
        keyspaceReplication.updateReplicationMode(5);

        keyspace.setKeyspaceReplication(keyspaceReplication);

        assertThat(keyspace.toString()).hasToString("CREATE KEYSPACE IF NOT EXISTS superName\n" +
                " WITH REPLICATION = {\n" +
                "'class': 'SimpleStrategy',\n" +
                "'replication_factor': 5\n" +
                "}\n" +
                "AND DURABLE_WRITES = true;");
    }


    @Test
    public void KeyspaceAndTenReplicasKeyspace() {
        Keyspace keyspace = new Keyspace("superName", false);
        KeyspaceReplication keyspaceReplication = new KeyspaceReplication(KeyspaceReplicationMode.SIMPLE_STRATEGY);
        keyspaceReplication.updateReplicationMode(10);

        keyspace.setKeyspaceReplication(keyspaceReplication);

        assertThat(keyspace.toString()).hasToString("CREATE KEYSPACE superName\n" +
                " WITH REPLICATION = {\n" +
                "'class': 'SimpleStrategy',\n" +
                "'replication_factor': 10\n" +
                "}\n" +
                "AND DURABLE_WRITES = true;");
    }

    @Test
    public void KeyspaceAndTenReplicasNoDurableWriteKeyspace() {
        Keyspace keyspace = new Keyspace("superName", false);
        KeyspaceReplication keyspaceReplication = new KeyspaceReplication(KeyspaceReplicationMode.SIMPLE_STRATEGY);
        keyspaceReplication.updateReplicationMode(10);

        keyspace.setKeyspaceReplication(keyspaceReplication);
        keyspace.setDurableWrite(false);

        assertThat(keyspace.toString()).hasToString("CREATE KEYSPACE superName\n" +
                " WITH REPLICATION = {\n" +
                "'class': 'SimpleStrategy',\n" +
                "'replication_factor': 10\n" +
                "}\n" +
                "AND DURABLE_WRITES = false;");
    }


    @Test
    public void KeyspaceCreateOnlyIfExistAndTenReplicasKeyspace_ReplicationNotDefined() {
        try {
            Keyspace keyspace = new Keyspace("superName", true);
            System.out.println(keyspace.toString());
            fail();
        } catch (KeyspaceReplicationNotDefinedException ignored) {
        }
    }
}